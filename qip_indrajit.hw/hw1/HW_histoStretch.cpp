// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_histoStretch:
//
// Apply histogram stretching to I1. Output is in I2.
// Stretch intensity values between t1 and t2 to fill the range [0,255].
//
// Written by: ADD YOUR NAMES HERE, 2016
//
void
HW_histoStretch(ImagePtr I1, int t1, int t2, ImagePtr I2)
{
    IP_copyImageHeader(I1, I2);
    int w = I1->width ();
    int h = I1->height();
    int total = w * h;
    
    // evaluate output: each input pixel indexes into lut[] to eval output
    
    // init lookup table


        
    int i, j, lut[MXGRAY];
    for(i=0; i<t1; ++i) lut[i] = 0;
   
    for (; i<t2; ++i) lut[i] = MaxGray *((double)(i - t1)/(t2-t1));
    
    for(j = MaxGray; j>=t2; --j) lut[j] = MaxGray;


    
        int type;
        ChannelPtr<uchar> p1, p2, endd;
        for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
            IP_getChannel(I2, ch, p2, type);
            for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
        }
    
}
