// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_quantize:
//
// Quantize I1 to specified number of levels. Apply dither if flag is set.
// Output is in I2.
//
// Written by: Indrajit Gurung, 2016
//
void
HW_quantize(ImagePtr I1, int levels, bool dither, ImagePtr I2)
{
    IP_copyImageHeader(I1, I2);
    int w = I1->width();
    int h = I1->height();
    int total = w * h;
    
    int type;
    ChannelPtr<uchar> p1, p2, endd;
    
    double scale = MXGRAY / levels;
    int bias = scale/2;
    int i, lut[MXGRAY];
    for(i=0; i<MXGRAY; ++i) //lut[i] = 0;
    {
        lut[i] = (scale * (int) (i/scale)) + bias;
    }
    
    
    if(dither){
        
        for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
            IP_getChannel(I2, ch, p2, type);

            int oscillation = 1;

            for(int y = 0; y<h; ++y){
                //check either the row is odd or even, oscillation is -ve if odd
                if(y%2){
                    oscillation = -1;
                } else {
                    oscillation = 1;
                }
                for(int x = 0; x<w; ++x){
                    int rand_value = ((double) rand()/RAND_MAX) * bias;
                    //jitter value added to the input pixel value and the value from lut is applied to val
                    int val = *p1++ + (rand_value * oscillation);
                    val = CLIP(val , 0, MaxGray);
                    *p2++ = lut[val];
                    oscillation *= -1;
                }

            }
        }
        
    }
    
    else{
        
        for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
            IP_getChannel(I2, ch, p2, type);
            for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
        }
    }
}
