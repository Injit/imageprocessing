// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_histoMatch:
//
// Apply histogram matching to I1. Output is in I2.
//
// Written by: Indrajit Gurung, 2016
//
void
HW_histoMatch(ImagePtr I1, ImagePtr Ilut, ImagePtr I2)
// {
//     IP_copyImageHeader(I1, I2);
//     int w = I1->width ();
//     int h = I1->height();
//     int total = w * h;
//     int i, j, p, R;
//     int left[MXGRAY], right[MXGRAY];
//     int Hsum=0, Havg, h1[MXGRAY];
//     double scale;
//     R = 0;
    
//     int type;
//     ChannelPtr<uchar> p1, p2, endd;
//     ChannelPtr<int> h2;
//  /* eval histogram */
    
    


//     for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
//         IP_getChannel(Ilut, ch, h2, type);//Ilut with h2 pointer
//         IP_getChannel(I2, ch, p2, type);
//         //            for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
//         //        }
//             /* evaluate remapping of all input gray levels;
//              Each input gray value maps to an interval of valid output values.
//              The endpoints of the intervals are left[] and right[] */
//         /* normalize h2 to conform with dimensions of I1 */
//         for(i=0; i<MXGRAY; i++) h1[i] = 0; /* clear histogram */
        
//         for(j=0; j<total; j++) h1[p1[j]]++;//histogram for input is created

//         for(i=Havg=0; i<MXGRAY; i++) {
//             Havg += h2[i];
//             //std::cout<<Havg<<std::endl;
//         }
        
//         scale = (double) total / Havg;
        
//             if(scale != 1)
//             {
//                 for(i=0; i<MXGRAY; i++)
//                 {
//                     h2[i] *= scale;
//                 }
//             }
        
//         for(i=0; i<MXGRAY; i++) {
//             /* left end of interval */
//             left[i] = R;
//             //std::cout<<"left["<<i<<"]  = "<<left[i]<<std::endl;
            
//             /* cumulative value for interval */
//             Hsum += h1[i];
//             //std::cout<<"h1["<<i<<"]  = "<<h1[i]<<std::endl;

//             //std::cout<<"   Hsum  "<<Hsum<<" value of h2["<<R<<"]  "<<h2[R]<<std::endl;

//             while(Hsum>=h2[R] && R<MaxGray) {
//                 //std::cout<<"   Hsum greater than h2[R]  "<<Hsum<<std::endl;

//                 /* compute width of interval */
//                 Hsum -= h2[R];
//                 //std::cout<<"Remaining Hsum  "<<Hsum<<std::endl;

//                 R++;
//             }

//             right[i] = R;
//             //std::cout<<"right["<<i<<"]  ="<<right[i]<<std::endl;

            
//         }
//             /* adjust Hsum as interval widens */
//             /* update */
//             /* init right end of interval */
//             /* clear h1 and reuse it below */
//     for(i=0; i<MXGRAY; i++) h1[i] = 0;
//             /* visit all input pixels */
//             for(i=0; i<total; i++) {
//                 p = left[p1[i]];//for intensity i
//                 //std::cout<<"i  "<<i<<"    p1["<<i<<"]  "<<p1[i]<<"   p  "<<p<<std::endl;
//                 if(h1[p] < h2[p]) /* mapping satisfies h2 */
//                 {
//                     p2[i] = p;
//                     if(p<2){
//                         std::cout<<"h1["<<p<<"]  "<<h1[p]<<"  h2[p] "<<h2[p]<<"  p2["<<i<<"]  "<<p<<std::endl;
//                     }
                    
//                 }
//                 else
//                     p2[i] = p = left[p1[i]] = MIN(p+1, right[p1[i]]);
                
//                 h1[p]++;
//             }
//         }
// }


{
   IP_copyImageHeader(I1, I2);
   int w = I1->width();
   int h = I1->height();
   /* total number of pixels in image */
   int total = w * h;
   
   IP_embedRange(I1,0,(double)MaxGray,I2);
   
   int len = Ilut->width();
   if (len <= MaxGray)
       fprintf(stderr,"IP_histogramMatch:warning %d\n",len);
       int *lut = new int[len];
       double *dd1 = new double[w];
       if(dd1 == NULL) IP_bailout("IP_histogramMatch: No memory");
           
           int R, type =0, luttype =0;
           
           
           int left[MXGRAY], right[MXGRAY],  indx[MXGRAY], lim[MXGRAY];
   int h1[MXGRAY], *h2;
   double hmin,hmax;
   
   ChannelPtr<uchar> p, p1, endd, lutp;
   for (int ch=0; IP_getChannel(I1, ch, p, type); ch++) {
       
       IP_getChannel(Ilut,ch,lutp,luttype);
       h2 = (int *) &lutp[0];
       
       
       
       
       int Hsum =0;
       for (int i =0 ; i<MXGRAY;i++) Hsum += h2[i];
           double scale = (double)total /Hsum;
           if (scale != 1){
               Hsum = 0;
               for(int i = 0; i<MXGRAY;i++){
                   h2[i] = ROUND(h2[i]*scale);
                   Hsum += h2[i];
                   if(Hsum > total){
                       h2[i] -= (Hsum - total);
                       for (; i<MXGRAY; i++) h2[i] =0;
                           
                           
                           }
                   
               }
               
           }
       // eval h1 and init left[], right[], and lim[]
       IP_histogram(I2, ch, h1, MXGRAY, hmin,hmax);
       R = Hsum =0;
       for(int i = 0; i<MXGRAY;i++){
           left[i] = indx[i] = R; // left end of interval
           lim[i] = h2[R] - Hsum; // leftover on left
           Hsum += h1[i]; //cum interval value
           
           // widen interval if Hsum >h2[R]
           while(Hsum > h2[R] && R <MaxGray){
               Hsum -= h2[R];
               R++;
               
           }
           
           right[i] = R;  // right end of interval
           
       }
       
       // clear h1 and reuse it below
       for(int i = 0; i<MXGRAY;i++) h1[i] =0;
           
           IP_getChannel(I2, ch, p, type);
           for(endd = p + total; p<endd;p++){
               int i = indx[*p];
               if (i == left[*p]){
                   
                   if (lim[*p]-- <= 0)
                       i = indx[*p] = MIN(i+1,MaxGray);
                       *p =i;
                       
                       
                       }else if(i<right[*p]){
                           
                           if(h1[i] < h2[i]) *p  = i;
                               else *p = indx[*p] = MIN(i+1,MaxGray);
                                   
                                   
                                   }else *p =i;
                                       h1[i]++;
               
               
           }
   }
   
   delete [] lut;
   delete [] dd1;
   
   
   
}

